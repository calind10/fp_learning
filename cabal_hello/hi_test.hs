module Main where

import Greeting
import Test.HUnit
import System.Exit

defaultTestCase = TestCase $ assertEqual "Greeting should be Hello World" "Hello World" greeting

main :: IO ()
main = do
    m <- runTestTT defaultTestCase
    if failures m == 0 then
        exitSuccess
    else
        exitFailure
