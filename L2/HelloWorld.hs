module Main where
import Text.ParserCombinators.Parsec hiding(spaces)
import Control.Monad
import System.Environment

data LispVal = Atom String
	| List [LispVal]
	| DottedList [LispVal] LispVal
	| Number Integer
	| String String
	| Bool Bool

instance Show LispVal where
	show (String s) = "\"" ++ s ++ "\""
	show (Atom at) = at
	show (Number n) = show n
	show (Bool True) = "#t"
	show (Bool False) = "#f"
	show (List l) = "(" ++ unwords (map show l) ++ ")"
	show (DottedList l v) = "(" ++ unwords (map show l) ++ " . " ++ show v ++ ")"

symbol :: Parser Char
symbol = oneOf "!#$%&|/*-+:<=>?@^_~"

spaces :: Parser()
spaces = skipMany1 space

parseString :: Parser LispVal
parseString = do
	char '"'
	x <- many $ noneOf "\""
	char '"'
	return $ String x

parseAtom :: Parser LispVal
parseAtom = do
	first <- letter <|> symbol
	rest <- many $ letter <|> digit <|> symbol
	let atom = first:rest
	return $ case atom of
		"#t" -> Bool True
		"#f" -> Bool False
		_ -> Atom atom

parseNumber :: Parser LispVal
parseNumber = liftM (Number . read) $ many1 digit

parseList :: Parser LispVal
parseList = liftM List $ sepBy parseLispVal spaces

parseDottedList :: Parser LispVal
parseDottedList = do
	head <- endBy parseLispVal spaces
	tail <- char '.' >> spaces >> parseLispVal
	return $ DottedList head tail

parseLispList :: Parser LispVal
parseLispList = do
	char '('
	x <- try parseList <|> parseDottedList
	char ')'
	return x

parseQuoted :: Parser LispVal
parseQuoted = do
	char '\''
	x <- parseLispVal
	return $ List [Atom "quote", x]

parseLispVal :: Parser LispVal
parseLispVal = parseAtom
	<|> parseNumber
	<|> parseString
	<|> parseQuoted
	<|> parseLispList

readExpr :: String -> String
readExpr input = case parse parseLispVal "lisp" input of
	Left err -> "No match: " ++ show err
	Right val -> "Found value:" ++ show val

main ::  IO ()
main = do
	args <- getArgs
	putStrLn(readExpr $ head args)
