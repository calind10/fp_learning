module Game2048 where

import Control.Monad.State
import Data.Matrix
import Safe
import System.Random

data Board = Board (Matrix Int)

instance Show Board where
    show (Board b) = "\n" ++ show b ++ "\n"

makeBoard :: Int -> Board
makeBoard n = Board $ zero n n 

boardSize :: Board -> Int
boardSize (Board b) = nrows b

transposeBoard :: Board -> Board
transposeBoard (Board b) = Board $ transpose b

getElement :: Board -> Int -> Int -> Int
getElement (Board b) line col = b ! (line, col)

setElement :: Board -> Int -> Int -> Int -> Board
setElement (Board b) line col value = Board $ setElem value (line, col) b

processRow :: [Int] -> [Int]
processRow l = actualProcess (length l) l
    where
        actualProcess :: Int -> [Int] -> [Int]
        actualProcess len (0:t) = actualProcess len t
        actualProcess len (f:n:t)
            | f == n = (2*f) : actualProcess (len - 1) t
            | n == 0 = actualProcess len (f:t)
            | otherwise = f : actualProcess (len - 1) (n:t)
        actualProcess len [x] = x : actualProcess (len - 1) []
        actualProcess len [] = replicate len 0 

shiftLeft :: Board -> Board
shiftLeft (Board b) = Board . fromLists $ map processRow (toLists b)

shiftRight :: Board -> Board
shiftRight (Board b) = Board . fromLists $ map (reverse . processRow . reverse) (toLists b)

shiftUp :: Board -> Board
shiftUp = transposeBoard . shiftLeft . transposeBoard 

shiftDown :: Board -> Board
shiftDown = transposeBoard . shiftRight . transposeBoard 

hasWon ::  Board -> Bool
hasWon (Board b) = any (== 2048) $ toList b 

hasLost :: Board -> Bool
hasLost (Board b) = all (/= 0) $ toList b

isOver :: Board -> Bool
isOver b = (hasWon b) || (hasLost b)

data GameState = Playin | Lost | Won deriving(Eq, Enum, Show)
type CurrentState = State GameState Board 

putNewElement :: RandomGen g => Board -> g -> (Board, g)
putNewElement b gen = do
    let (i, gen2) = randomR (1, boardSize b) gen
    let (j, gen3) = randomR (1, boardSize b) gen2
    if getElement b i j == 0 then
        (setElement b i j 2, gen3)
    else
        putNewElement b gen3

toAction :: Char -> Maybe (Board -> Board)
toAction c
    | c == 'l' = Just shiftLeft
    | c == 'r' = Just shiftRight
    | c == 'u' = Just shiftUp
    | c == 'd' = Just shiftDown
    | otherwise = Nothing

tick :: RandomGen g => Board -> g -> IO ()
tick b gen = do
    putStrLn $ show b
    putStrLn "Make a move"
    line <- getLine
    let selectedAction = headMay line >>= toAction
    case selectedAction of
        Nothing -> do 
            putStrLn "Invalid input"
            tick b gen
        Just action ->
            let board = action b in
            if hasWon board then
                putStrLn "Congratulations! You won!"
            else if hasLost board then
                putStrLn "You've lost :("
            else do
                let (b2, gen2) = putNewElement board gen
                tick b2 gen2
