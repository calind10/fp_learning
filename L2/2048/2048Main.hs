module Main where

import Game2048
import System.Random
import System.IO

initBoard :: Board
initBoard = setElement (setElement (setElement (makeBoard 6) 1 1 2) 1 2 2) 2 2 2

main = do
    g <- newStdGen
    let (board, g2) = putNewElement initBoard g 
    putStrLn "2048. Create a tile with the number 2048"
    putStrLn "\t u - Move tiles UP"
    putStrLn "\t d - Move tiles DOWN"
    putStrLn "\t l - Move tiles LEFT"
    putStrLn "\t r - Move tiles RIGHT"
    tick board g2
