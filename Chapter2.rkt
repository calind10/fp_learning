#lang racket

;(define (linear-application x y a b)
;  (add (mul a x) (mul b y)))

(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y)) (* (denom x) (numer y)) )
            (* (denom x) (denom y))))

(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y)) (* (denom x) (numer y)) )
            (* (denom x) (denom y))))

(define (mul-rat x y)
  (make-rat (* (numer x) (numer y))
            (* (denom x) (denom y))))

(define (div-rat x y)
  (make-rat (* (numer x) (denom y))
            (* (denom x) (numer y))))

(define (equal-rat? x y)
  (= (* (numer x) (denom y))
     (* (denom x) (numer y))))

(define (make-rat n d)
  (let ([g (gcd n d)])
    (cons (/ n g) (/ d g))))
(define numer car)
(define denom cdr)

(define (print-rat r)
  (newline)
  (display (numer r))
  (display "/")
  (display (denom r)))

(define (install-rect-package)
  (define (real-part z) (car z))
  (define (tag z) (cons 'rectangular z))
  (put 'real-part 'rectangular real-part))

(define (apply-generic op . args)
  (let ([type-tags (map car args)]
        [actual-op (get op type-tags)])
    (if actual-op (apply actual-op (map cdr args))
        (error "Unknown operation found for:" (list op type-tags)))))

