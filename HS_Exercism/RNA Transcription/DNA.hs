module DNA
    (toRNA) where

dNAtoRNA :: Char -> Char
dNAtoRNA x
    | x == 'G' = 'C'
    | x == 'C' = 'G'
    | x == 'T' = 'A'
    | x == 'A' = 'U'

toRNA :: String -> String
toRNA = map dNAtoRNA
