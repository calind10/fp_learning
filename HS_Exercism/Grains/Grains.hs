module Grains(total, square) where

square :: Integral a => a -> a
square 1 = 1
square x = 2 * square $ x-1

total :: Integral a => a 
total = foldr (+) 0 $ map square [1..64]
